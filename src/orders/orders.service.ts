import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRespository: Repository<Order>,
    @InjectRepository(Customer)
    private customersRespository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRespository: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemsRespository: Repository<OrderItem>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const customer = await this.customersRespository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.ordersRespository.save(order); //ได้ id

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.productsRespository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order;
      await this.orderItemsRespository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }

    await this.ordersRespository.save(order);
    return await this.ordersRespository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRespository.find({
      relations: ['customer', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.ordersRespository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.ordersRespository.findOneBy({ id: id });
    return this.ordersRespository.softRemove(order);
  }
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRespository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRespository.save(createProductDto);
  }

  findAll() {
    return this.productsRespository.find({});
  }

  async findOne(id: number) {
    const product = await this.productsRespository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRespository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    const updateProduct = { ...product, ...updateProductDto };
    return this.productsRespository.save(updateProduct);
  }

  async remove(id: number) {
    const product = await this.productsRespository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.productsRespository.softRemove(product);
  }
}

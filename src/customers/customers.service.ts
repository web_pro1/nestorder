import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRespository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customersRespository.save(createCustomerDto);
  }

  findAll() {
    return this.customersRespository.find({});
  }

  async findOne(id: number) {
    const customer = await this.customersRespository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRespository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRespository.save(updateCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRespository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customersRespository.softRemove(customer);
  }
}
